#!/bin/bash -eu

#Gnome Terminal configs:

#backup current:
#dconf dump /org/gnome/terminal/ > "$HOME"/.config/gnome-terminal/preferences.txt

if [ -n "$DISPLAY" ]
then
	#load backup:
	dconf load /org/gnome/terminal/ < "$HOME"/.config/gnome-terminal/preferences.txt
fi

